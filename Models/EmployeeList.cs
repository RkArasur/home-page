﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Employees.Repos;
namespace Employees.Models
{
    public class EmployeeList
    {
        public int empid { get; set; }
        public string fname { get; set; }
        public string lname { get; set; }
        public string mail { get; set; }
    }
}
