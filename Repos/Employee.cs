﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Employees.Repos
{
    public class Employee
    {
        private string firstname;
        private string lastname;
        private string mail;
        private int EmpID;
        
        public Employee()
        {
        }

        public void setInfo(int eid,string fname,string lname,string mail)
        {
            this.EmpID = eid;
            this.firstname = fname;
            this.lastname = lname;
            this.mail = mail;
        }
        public string[] getInfo()
        {
            string[] e = new string[] { this.EmpID.ToString(), this.firstname, this.lastname, this.mail };
            return e;
        }
    }
}
