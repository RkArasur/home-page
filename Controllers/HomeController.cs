﻿using Employees.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Employees.Repos;

namespace Employees.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            List<EmployeeList> emps = new List<EmployeeList>();
            Employee[] employees = new Employee[10];
            string[] fnames = new string[10] { "ravi", "ram", "lakshman", "tony", "cillian", "john", "michael", "lewis", "cody", "coby" };
            string[] lnames = new string[10] { "kiran", "Rajath", "vira", "stark", "murphy", "wick", "scott", "hamilton", "jones", "cotton" };
            int[] ids = new int[10] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            string[] mails = new string[10] { "rk@gmail.com", "ram@gmail.com", "lakshman@hotmail.com", "tony@starkindustries.com", "cillian@gmail.com", "john@parabellum.com", "scott@rediffmail.com", "lewis@f1.com", "cody@dp.com", "coby@dp.com" };
            for (int i = 0; i < 10; i++)
            {
                employees[i] = new Employee();
                employees[i].setInfo(ids[i], fnames[i], lnames[i], mails[i]);
            }
            for(int i = 0; i < 10; i++)
            {
                string[] res = employees[i].getInfo();
                int eid = int.Parse(res[0]);
                string fn = res[1], ln = res[2], mid = res[3];
                emps.Add(new EmployeeList
                {
                    empid = eid,
                    fname = fn,
                    lname = ln,
                    mail = mid
                });
            }
            ViewBag.Emps = emps.ToArray();
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}